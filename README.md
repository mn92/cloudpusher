# README #

### What is CloudPusher? ###

The concept of the CloudPusher website is a simple one; a photography portal for viewing, posting and classifying photos of cloud formations. 
The site is aimed at photographers, artists and anyone wishing to find some images of clouds. 
Currently a proof of concept.

### Where is CloudPusher? ###

Online now on http://www.cloudpusher.tk
Check it out!