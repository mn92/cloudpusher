
$(".thumbnail").click(function(){
    var image = $(this).find("img").attr("src");
    var author = $(this).find("img").attr("data-author");
    var location = $(this).find("img").attr("data-location");
    var date = $(this).find("img").attr("data-date");
    var description = $(this).find("img").attr("data-description");
    $("#cloudModal").find("img").attr("src",image);
    $("#img-auth").text(author);
    $("#img-loc").text(location);
    $("#img-date").text(date);
    $("#img-desc").text(description);
    $("#cloudModal").modal("show");
});

$("#loginBtn").click(function(){
    $("#loginModal").modal("show");
});